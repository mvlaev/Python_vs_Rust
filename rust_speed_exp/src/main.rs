fn work() {
    let v: Vec<i128> = (1..10000000).collect();

    let mut sum = 0;
    let mut count = 0;

    for i in &v {
        sum += i;
        count += 1;
    }

    let promedio = sum/count;

    let mut sum_of_sq_dif = 0;

    for i in &v {
        sum_of_sq_dif += (i - promedio) * (i - promedio);
    }

    let sigma_sq = sum_of_sq_dif/count;
    let sigma = (sigma_sq as f64).sqrt();
    println!("{sigma}");
}

fn main() {
//     for _i in 1..2 {
//         work();
//     }
    work();
}

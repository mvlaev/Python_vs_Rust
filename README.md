# ***Python vs Rust :)***

*NOTE: THIS IS MERELY AN EXPERIMENT, IT IS NOT INTENDED TO BE A (REAL, EXTENSIVE, CORRECT....) BENCHMARK! It's just a product of curiosity and having fun with my favourite programming languages!*

These are two simple programs that calculate the standard deviation of the numbers from 1 to 10 000 000, which is a complete nonsense from statistics point of view BUT presents some load to the machine so that the time that the calculation takes could be measured.

The two programs follow the exact same algorithm, the only difference is that one is written in Python and the other in Rust.

If you decide to compare both, don't forget to build the Rust code with release optimisations on and actually run the release binary :)



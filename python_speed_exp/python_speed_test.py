from math import sqrt

def work():
	#a = [i for i in range(10000000)]
	a = list(range(10000000))

	#find the sum(a)/len(a) manually
	s = 0
	count = 0

	for i in a:
		s += i
		count += 1

	promedio = s/count

	#find the promedio of the sum of the squared differences
	sum_of_sq_dif = 0

	for i in a:
		sum_of_sq_dif += (i - promedio) * (i - promedio)

	sigma_sq = sum_of_sq_dif/count
	sigma = sqrt(sigma_sq)

	print(sigma)

#for i in range(1):
work()
